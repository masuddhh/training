#build stage
FROM golang:1.18-alpine as builder

WORKDIR /app
COPY . .

RUN go mod tidy 
RUN go build -o main main.go

#Run stage
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=builder /app/main .

CMD ["./main"]



